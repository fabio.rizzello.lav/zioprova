ThisBuild / scalaVersion     := "2.13.5"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "provazio",
    scalacOptions += "-Ymacro-annotations",
    libraryDependencies ++= Seq(
    "dev.zio" %% "zio" % "1.0.5",
    "dev.zio" %% "zio-macros" % "1.0.5"

    )
  )
