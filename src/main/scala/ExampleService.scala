import zio.macros.accessible
import zio.{Has, UIO, ZIO, ZLayer}

@accessible
object ExampleService {

  type ExampleService = Has[Service]

  trait Service {
    def prova(): UIO[Int]
    def prova2(): UIO[Int]
  }

  val live: ZLayer[Any, Nothing, ExampleService] = ZLayer.succeed(new Service {
    override def prova(): UIO[Int] = ZIO.succeed(1)

    override def prova2(): UIO[Int] = ZIO.succeed(2)
  })
}